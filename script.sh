#! /usr/bin/env bash

echo "Hello World"

# lookup feature, provides a variable used to lookup different files
# Run time parameters
LOOKUP_VAL=F1
LOOKUP_TABLE=lookup.txt

# Extract required feature from LUT
#grep and regex (sed -rn) used to lookup particular features in files saved
LOOKUP_RESULT()=`grep "F1" ./lookup.txt | sed -rn 's/[a-zA-Z0-9]+ ([a-zA-Z0-9]+)/\1/p'`
echo "LOOKUP_RESULT: $LOOKUP_RESULT"

lookup_value="$(LOOKUP_RESULT)"
echo $LOOKUP_RESULT
# if this directory exists

if [ -d $LOOKUP_RESULT ]
then 
    echo "Directory found"
    exit 0
else 
    # if this directory doesn't exist
    echo "Directory not found"
    # Do other things here
    # terminates if loop
fi
}

#Excluding files beginning with ','
"files.exclude": {
"**/." : true
},
#searches for the files throughout all folders
"search.exclude": {
    "**/node_modules": true
    "**/bower_components": true
},

#Renaming function will contain code used to rename different files
#This can be called anytime in the code
Renaming_feature()[
    #Renaming_feature variable has just been created
    folder_chose=$1
    new_folder=$2
    #these variables store arguments input by the user
    file=$(pwd)/
    #The file variable will check the existing depository for the same file names
    if [ -d $file/new_folder ]; then
        #if matching folder found, new folder will not be created
        echo "File cannot be created as a file with that name currently exists"
    else
        #if matching folder not found the new folder will be created
        mv "$folder_chose/",  "$new_folder/"
        #updates gitlab with name change and final result
        #make updating_to_csgitlab
        #moves the chosen folder to the new folder location
    fi

]}

#creates a file search algorithm named file_search
file_search(){
~/CS1PC20/file_search $1
if [ -f path ]
then
    path1=$(<path)
    rm path
    ~/CS1PC20/file_search $2
else
    #if folder not found this will be output to the user
    echo "Folder does not exist"
fi
}

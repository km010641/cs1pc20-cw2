#! /usr/bin/env bash

echo "Hello World"

# lookup feature, provides a variable used to lookup different files
# Run time parameters
LOOKUP_VAL=F1
LOOKUP_TABLE=lookup.txt

# Extract required feature from LUT
#grep and regex (sed -rn) used to lookup particular features in files saved
LOOKUP_RESULT()=`grep "F1" ./lookup.txt | sed -rn 's/[a-zA-Z0-9]+ ([a-zA-Z0-9]+)/\1/p'`
echo "LOOKUP_RESULT: $LOOKUP_RESULT"

lookup_value="$(LOOKUP_RESULT)"
echo $LOOKUP_RESULT
# if this directory exists

if [ -d $LOOKUP_RESULT ]
then 
    echo "Directory found"
    exit 0
else 
    # if this directory doesn't exist
    echo "Directory not found"
    
    # Do other things here
    # terminates if loop
fi
}

#Excluding files beginning with ','
"files.exclude": {
"**/." : true
},
#searches for the files throughout all folders
"search.exclude": {
    "**/node_modules": true
    "**/bower_components": true
},

LOOKUP_RESULT
echo $

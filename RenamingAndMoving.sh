#Renaming function will contain code used to rename different files
#This can be called anytime in the code
Renaming_feature()[
    #Renaming_feature variable has just been created
    folder_chose=$1
    new_folder=$2
    #these variables store arguments input by the user
    file=$(pwd)/
    #The file variable will check the existing depository for the same file names
    if [ -d $file/new_folder ]; then
        #if matching folder found, new folder will not be created
        echo "File cannot be created as a file with that name currently exists"
    else
        #if matching folder not found the new folder will be created
        mv "$folder_chose/",  "$new_folder/"
        #updates gitlab with name change and final result
        #make updating_to_csgitlab
        #moves the chosen folder to the new folder location
    fi

]}

Renaming_feature
echo $

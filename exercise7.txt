# Coursework 2 Project Planning exercise

Planning is the keystone of successful programming, but it need not all be dry paperwork.  The idea of experimenting with code in a series of feasibility studies can really help identify which approach to take.

In this exercise, you have some thinking to do, and you should get together with others in your peer group (other students) to discuss ideas.

First, though, grab a copy of this project by going to it on CSGitLab and using the "Fork" button near the top right corner.

## Goals 

Identify the key goals you need to complete for the coursework


=======
1. Ensure the most tasks are completed
1. Ensure all the code can be seen and read
1. Ensure the code is commented well
1. Ensure the svg can be seen on gitlab
>>>>>>> d7668e8230253857bb2f817bec6d07492d1c122e


## Requirements to fulfill goals

What do you need to be able to meet those goals?  This can include clearer specifications, tests (what needs testing, how to test it?), knowledge etc.

1. Videos
1. better knowledge of coding


## Plan:

### What experimental code can I write now? (feasibility studies)

1. lookup code 
1. gantt chart svg code
